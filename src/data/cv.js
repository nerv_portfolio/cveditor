{
  cvs: [
    {
      cv: {
        firstname: '',
        lastname: '',
        address: '',
        email: '',
        phone: '',
        socialnetworks: [
          {
            logo: '',
            name: '',
            link: '',
          }
        ],
        position: '',
        skills: {
          programlanguages: [],
          technologies: [],
          databases: [],
          operationsystems: [],
          tools: {
            testingtools: []
          }
        },
        workhistory: [
          {
            company: '',
            history: [
              {
                position: '',
                startdate:'',
                enddate:'',
              }
            ]
          }
        ],
        certificates: [
          {
            company: '',
            certificates: [
              {
                date: '',
                name: '',
                link: ''
              }
            ]
          }
        ],
        esucation: [
          {
            university: '',
            diplomas: [
              {
                faculty: '',
                speciality: ''
                startdate: '',
                enddate:''
                degree: ''
              }
            ]
          }
        ],
        languages: [
          {
            name: '',
            level: ''
          }
        ]
      },
    }
  ]
}